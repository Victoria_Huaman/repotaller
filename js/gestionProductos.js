var productosObtenidos;/*tomara toda la informacion del json*/

function getProductos(){

 var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
 var request = new XMLHttpRequest();
/*configuracion de la peticion*/
 request.onreadystatechange = function(){
   /*200 respondio bien, status 4= cargado, continuar*/
   if (this.readyState == 4 && this.status == 200) {
     /*pintar el resultado en la cosnole f12*/
       console.log(request.responseText);
       productosObtenidos = request.responseText;
       procesarProductos();
   }
 }

 request.open("GET",url,true);
 request.send();

}

function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);
  var divTabla = document.getElementById("divTabla");//traer objeto del html
  var tabla = document.createElement("table");//crear un elemento html de tipo table
  var tbody = document.createElement("tbody");//crear un elemento html de tipo tbody

  tabla.classList.add("table");
  tabla.classList.add("table-striped");//pintar tablas en raya

  for (var i = 0; i < JSONProductos.value.length; i++) {
   var nuevafila = document.createElement("tr");
   var columnaNombre = document.createElement("td"); //tabledate
   columnaNombre.innerText = JSONProductos.value[i].ProductName;
   var columnaPrecio = document.createElement("td"); //tabledate
   columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
   var columnaStock = document.createElement("td"); //tabledate
   columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

   nuevafila.appendChild(columnaNombre);
   nuevafila.appendChild(columnaPrecio);
   nuevafila.appendChild(columnaStock); //agregar elementos a la fila

   tbody.appendChild(nuevafila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
