var clientesObtenidos;/*tomara toda la informacion del json*/

function getClientes(){

 var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
 var request = new XMLHttpRequest();
/*configuracion de la peticion*/
 request.onreadystatechange = function(){
   /*200 respondio bien, status 4= cargado, continuar*/
   if (this.readyState == 4 && this.status == 200) {
     /*pintar el resultado en la cosnole f12*/
       console.log(request.responseText);
       clientesObtenidos = request.responseText;
       procesarClientes();
   }
 }

 request.open("GET",url,true);
 request.send();

}


function procesarClientes(){
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("divClientes");//traer objeto del html
  var tabla = document.createElement("table");//crear un elemento html de tipo table
  var tbody = document.createElement("tbody");//crear un elemento html de tipo tbody

  tabla.classList.add("table");
  tabla.classList.add("table-striped");//pintar tablas en raya

  for (var i = 0; i < JSONClientes.value.length; i++) {
   var nuevafila = document.createElement("tr");
   var columnaNombre = document.createElement("td"); //tabledate
   columnaNombre.innerText = JSONClientes.value[i].ContactName;
   var columnaCiudad = document.createElement("td"); //tabledate
   columnaCiudad.innerText = JSONClientes.value[i].City;
   var columnaBandera = document.createElement("td"); //tabledate
   var imgBandera = document.createElement("img");

   imgBandera.classList.add("flag");

   if(JSONClientes.value[i].Country == "UK"){
     imgBandera.src = rutaBandera + "United-Kingdom.png";
   }else{
     imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
   }

   columnaBandera.appendChild(imgBandera);

   nuevafila.appendChild(columnaNombre);
   nuevafila.appendChild(columnaCiudad);
   nuevafila.appendChild(columnaBandera); //agregar elementos a la fila

   tbody.appendChild(nuevafila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
